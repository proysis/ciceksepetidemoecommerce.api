using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using CicekSepeti.ECommerce.Core.Entities;

namespace CicekSepeti.ECommerce.Business.Abstract
{
    public interface IBaseService<TEntity> where TEntity : class, IEntity, new()
    {
        List<TEntity> GetAll();
        Task<TEntity> GetByIdAsync(Guid id);
        TEntity GetById(Guid id);
        TEntity GetById(Guid id, Expression<Func<TEntity, object>> including);
        void Add(TEntity item);
        void Update(TEntity item);
        void Delete(TEntity item);
    }
}