using System;
using System.Collections.Generic;
using CicekSepeti.ECommerce.Entity.Concrete.Tables;
using CicekSepeti.ECommerce.Entity.Concrete.Views;

namespace CicekSepeti.ECommerce.Business.Abstract
{
    public interface IBasketProductService : IBaseService<BasketProduct>
    {
        List<BasketProduct> GetListByBasketId(Guid basketId);

        void AddOrUpdateBasketProduct(BasketProduct basketProduct);

        BasketProduct GetBasketProduct(Guid basketId, Guid productId);
        
        ViewBasketProduct GetBasketProductView(Guid userId, Guid productId);

        void AddListToBasket(List<BasketProduct> basketProducts, Guid basketId);
    }
}