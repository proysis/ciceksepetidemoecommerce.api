using System;
using System.Threading.Tasks;
using CicekSepeti.ECommerce.Entity.Concrete.Tables;

namespace CicekSepeti.ECommerce.Business.Abstract
{
    public interface IBasketService : IBaseService<Basket>
    {
        Task<Basket> GetBasketByUserIdAsync(Guid userId);
        Basket GetBasketByUserId(Guid userId);

        Basket SetBasket(Guid userId);
    }
}