using System.Threading.Tasks;
using Microsoft.SqlServer.Management.Smo;

namespace CicekSepeti.ECommerce.Business.Abstract
{
    public interface IDataBaseService
    {
        void CreateDataBase();
    }
}