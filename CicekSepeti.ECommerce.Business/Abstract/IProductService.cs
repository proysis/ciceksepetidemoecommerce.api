using CicekSepeti.ECommerce.Entity.Concrete.Tables;

namespace CicekSepeti.ECommerce.Business.Abstract
{
    public interface IProductService : IBaseService<Product>
    {
    }
}