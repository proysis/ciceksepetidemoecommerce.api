using System;
using System.Collections.Generic;
using CicekSepeti.ECommerce.Entity.Concrete.Tables;
using CicekSepeti.ECommerce.Entity.Concrete.Views;

namespace CicekSepeti.ECommerce.Business.Abstract
{
    public interface IUserSecurityRoleService : IBaseService<UserSecurityRole>
    {
        List<ViewUserSecurityRole> GetUserSecurityRoles(Guid userId);
    }
}