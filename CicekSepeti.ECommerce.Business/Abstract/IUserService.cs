using System;
using System.Threading.Tasks;
using CicekSepeti.ECommerce.Entity.Concrete.Tables;

namespace CicekSepeti.ECommerce.Business.Abstract
{
    public interface IUserService : IBaseService<User>
    {
        Task<User> GetByUserNameAndPassword(string userName, string password);
        bool ValidaUser(Guid userId, string userToken);
    }
}