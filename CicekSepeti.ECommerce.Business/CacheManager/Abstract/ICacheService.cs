using System;
using CicekSepeti.ECommerce.Core.CacheAccess;
using CicekSepeti.ECommerce.Entity.Concrete.Tables;
using CicekSepeti.ECommerce.Entity.Models;

namespace CicekSepeti.ECommerce.Business.CacheManager.Abstract
{
    public interface ICacheService : ICacheBase
    {
        void SetNewUserToken(Guid userId, string value);
        string GetUserToken(string key);
    }
}