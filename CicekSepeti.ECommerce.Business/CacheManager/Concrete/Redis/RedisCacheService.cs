using System;
using CicekSepeti.ECommerce.Business.CacheManager.Abstract;
using CicekSepeti.ECommerce.Core.CacheAccess.Redis;

namespace CicekSepeti.ECommerce.Business.CacheManager.Concrete.Redis
{
    public class RedisCacheService : RedisCacheBase, ICacheService
    {
        public void SetNewUserToken(Guid userId, string value)
        {
            string key = CacheType.UserToken + userId;
            Add(key, value);
        }

        public string GetUserToken(string key)
        {
            var value = GetByKey<string>(key);

            return value;
        }
    }
}