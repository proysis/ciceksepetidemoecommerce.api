using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using CicekSepeti.ECommerce.Business.Abstract;
using CicekSepeti.ECommerce.Core.Entities;
using CicekSepeti.ECommerce.Core.Extensions;
using CicekSepeti.ECommerce.DataAccess.Abstract;

namespace CicekSepeti.ECommerce.Business.Concrete
{
    public class BaseService<TDal, TEntity> : IBaseService<TEntity> where TDal : class, IBaseDal<TEntity> where TEntity : class, IEntity, new()
    {
        protected readonly TDal _dal;

        protected BaseService(TDal dal)
        {
            _dal = dal;
        }

        public List<TEntity> GetAll()
        {
            return _dal.GetList();
        }

        public async Task<TEntity> GetByIdAsync(Guid id)
        {
            return await _dal.GetAsync(t => t.GetPropertyValue<TEntity, Guid>("Id") == id);
        }

        public TEntity GetById(Guid id)
        {
            return _dal.Get(t => t.GetPropertyValue<TEntity, Guid>("Id") == id);
        }

        public TEntity GetById(Guid id, Expression<Func<TEntity, object>> including)
        {
            return _dal.GetWithIncluding(t => t.GetPropertyValue<TEntity, Guid>("Id") == id, including);
        }

        public void Add(TEntity item)
        {
            _dal.Add(item);
        }

        public void Update(TEntity item)
        {
            _dal.Update(item);
        }

        public void Delete(TEntity item)
        {
            _dal.Delete(item);
        }
    }
}