using System;
using System.Collections.Generic;
using CicekSepeti.ECommerce.Business.Abstract;
using CicekSepeti.ECommerce.Core.Extensions;
using CicekSepeti.ECommerce.DataAccess.Abstract;
using CicekSepeti.ECommerce.Entity.Concrete.Tables;
using CicekSepeti.ECommerce.Entity.Concrete.Views;
using CicekSepeti.ECommerce.Entity.Models;
using Microsoft.AspNetCore.Http;

namespace CicekSepeti.ECommerce.Business.Concrete
{
    public class BasketProductService : BaseService<IBasketProductDal, BasketProduct>, IBasketProductService
    {
        public BasketProductService(IBasketProductDal dal) : base(dal)
        {
        }

        public List<BasketProduct> GetListByBasketId(Guid basketId)
        {
            return _dal.GetList(t => t.BasketId == basketId);
        }

        public void AddOrUpdateBasketProduct(BasketProduct basketProduct)
        {
            bool hasItem = _dal.Any(t => t.BasketId == basketProduct.BasketId && t.ProductId == basketProduct.ProductId);
            
            if (hasItem)
            {
                _dal.Update(basketProduct);
            }
            else
            {
                _dal.Add(basketProduct);
            }
        }

        public BasketProduct GetBasketProduct(Guid basketId, Guid productId)
        {
            return _dal.Get(t => t.BasketId == basketId && t.ProductId == productId);
        }
        public ViewBasketProduct GetBasketProductView(Guid userId, Guid productId)
        {
            return _dal.GetView<ViewBasketProduct>(t=>t.UserId == userId && t.ProductId == productId);
        }

        public void AddListToBasket(List<BasketProduct> basketProducts, Guid basketId)
        {
            _dal.AddProductListToBasket(basketProducts, basketId);
        }
    }
}