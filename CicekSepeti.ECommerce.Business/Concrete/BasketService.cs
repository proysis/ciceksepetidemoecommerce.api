using System;
using System.Threading.Tasks;
using CicekSepeti.ECommerce.Business.Abstract;
using CicekSepeti.ECommerce.Core.Extensions;
using CicekSepeti.ECommerce.DataAccess.Abstract;
using CicekSepeti.ECommerce.Entity.Concrete.Tables;

namespace CicekSepeti.ECommerce.Business.Concrete
{
    public class BasketService : BaseService<IBasketDal, Basket>, IBasketService
    {
        public BasketService(IBasketDal dal) : base(dal)
        {
        }

        public Basket GetBasketByUserId(Guid userId)
        {
            return _dal.Get(t => t.UserId == userId);
        }
        public async Task<Basket> GetBasketByUserIdAsync(Guid userId)
        {
            return await _dal.GetAsync(t => t.UserId == userId);
        }

        public Basket SetBasket(Guid userId)
        {
            Basket basket = GetBasketByUserId(userId);
                
            bool hasBasket = basket.HasValue();

            if (hasBasket)
            {
                return basket;
            }
            
            basket = new Basket
            {
                UserId = userId,
                Date = DateTime.Now
            };
            
            Add(basket);
            
            return basket;
        }
    }
}