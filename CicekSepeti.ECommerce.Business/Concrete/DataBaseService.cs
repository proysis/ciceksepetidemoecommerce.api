using CicekSepeti.ECommerce.Business.Abstract;
using CicekSepeti.ECommerce.DataAccess.Abstract;

namespace CicekSepeti.ECommerce.Business.Concrete
{
    public class DataBaseService : IDataBaseService
    {
        private readonly IDataBase _dataBase;

        public DataBaseService(IDataBase dataBase)
        {
            _dataBase = dataBase;
        }

        public void CreateDataBase()
        {
            _dataBase.Create();
        }
    }
}