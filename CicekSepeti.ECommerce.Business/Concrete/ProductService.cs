using CicekSepeti.ECommerce.Business.Abstract;
using CicekSepeti.ECommerce.DataAccess.Abstract;
using CicekSepeti.ECommerce.Entity.Concrete.Tables;

namespace CicekSepeti.ECommerce.Business.Concrete
{
    public class ProductService : BaseService<IProductDal, Product>, IProductService
    {
        public ProductService(IProductDal dal) : base(dal)
        {
        }
    }
}