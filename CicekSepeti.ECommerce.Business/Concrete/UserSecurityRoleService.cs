using System;
using System.Collections.Generic;
using CicekSepeti.ECommerce.Business.Abstract;
using CicekSepeti.ECommerce.DataAccess.Abstract;
using CicekSepeti.ECommerce.Entity.Concrete.Tables;
using CicekSepeti.ECommerce.Entity.Concrete.Views;

namespace CicekSepeti.ECommerce.Business.Concrete
{
    public class UserSecurityRoleService : BaseService<IUserSecurityRoleDal, UserSecurityRole>, IUserSecurityRoleService
    {
        public UserSecurityRoleService(IUserSecurityRoleDal dal) : base(dal)
        {
        }

        public List<ViewUserSecurityRole> GetUserSecurityRoles(Guid userId)
        {
            return _dal.GetViewList<ViewUserSecurityRole>(t => t.UserId == userId);
        }
    }
}