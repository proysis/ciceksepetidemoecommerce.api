

using System;
using System.Threading.Tasks;
using CicekSepeti.ECommerce.Business.Abstract;
using CicekSepeti.ECommerce.Core.Extensions;
using CicekSepeti.ECommerce.DataAccess.Abstract;
using CicekSepeti.ECommerce.Entity.Concrete.Tables;

namespace CicekSepeti.ECommerce.Business.Concrete
{
    public class UserService : BaseService<IUserDal, User>, IUserService
    {
        public UserService(IUserDal dal) : base(dal)
        {
        }


        public Task<User> GetByUserNameAndPassword(string userName, string password)
        {
            return _dal.GetAsync(t => t.UserName == userName && t.Password == password.ToMd5());
        }

        public bool ValidaUser(Guid userId, string userToken)
        {
            return _dal.Any(t => t.Id == userId && t.UserToken == userToken);
        }
    }
}