using CicekSepeti.ECommerce.Business.Abstract;
using CicekSepeti.ECommerce.Business.CacheManager.Abstract;
using CicekSepeti.ECommerce.Business.CacheManager.Concrete.Memory;
using CicekSepeti.ECommerce.Business.CacheManager.Concrete.Redis;
using CicekSepeti.ECommerce.Business.Concrete;
using CicekSepeti.ECommerce.DataAccess.Abstract;
using CicekSepeti.ECommerce.DataAccess.Concrete.EntityFramework;
using Ninject.Modules;

namespace CicekSepeti.ECommerce.Business.DependencyResolver.Ninject
{
    public class BusinessModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IUserService>().To<UserService>().InSingletonScope();
            Bind<IUserDal>().To<EFUserDal>().InSingletonScope();
            
            Bind<IUserSecurityRoleService>().To<UserSecurityRoleService>().InSingletonScope();
            Bind<IUserSecurityRoleDal>().To<EFUserSecurityRoleDal>().InSingletonScope();
            
            Bind<IBasketService>().To<BasketService>().InSingletonScope();
            Bind<IBasketDal>().To<EFBasketDal>().InSingletonScope();
            
            Bind<IBasketProductService>().To<BasketProductService>().InSingletonScope();
            Bind<IBasketProductDal>().To<EFBasketProductDal>().InSingletonScope();
            
            Bind<IProductService>().To<ProductService>().InSingletonScope();
            Bind<IProductDal>().To<EFProductDal>().InSingletonScope();

            Bind<ICacheService>().To<MemoryCacheService>().InSingletonScope();
            
            Bind<IDataBaseService>().To<DataBaseService>().InSingletonScope();
            Bind<IDataBase>().To<EFDataBase>().InSingletonScope();
        }
    }
}