using CicekSepeti.ECommerce.Business.Abstract;
using CicekSepeti.ECommerce.Business.CacheManager.Abstract;
using CicekSepeti.ECommerce.Business.CacheManager.Concrete.Memory;
using CicekSepeti.ECommerce.Business.Fake;
using Ninject.Modules;

namespace CicekSepeti.ECommerce.Business.DependencyResolver.Ninject
{
    public class BusinessModuleFake : NinjectModule
    {
        public override void Load()
        {
            Bind<IBasketService>().To<BasketServiceFake>();
            Bind<IBasketProductService>().To<BasketProductServiceFake>();
            Bind<IProductService>().To<ProductServiceFake>();
        }
    }
}