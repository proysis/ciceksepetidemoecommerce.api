using System.Threading.Tasks;

namespace CicekSepeti.ECommerce.Business.DependencyResolver.Ninject.Factory
{
    public interface IServiceFactory
    {
        TService GetService<TService>();
        void Release<TService>(TService item);
    }
}