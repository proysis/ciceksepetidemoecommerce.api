using Ninject;

namespace CicekSepeti.ECommerce.Business.DependencyResolver.Ninject.Factory
{
    public class ServiceFactory : IServiceFactory
    {
        private readonly IKernel _kernel;

        public ServiceFactory()
        {
            _kernel = new StandardKernel();
            _kernel.Load<BusinessModule>();
        }

        public TService GetService<TService>()
        {
            return _kernel.Get<TService>();
        }

        public void Release<TService>(TService item)
        {
            _kernel?.Release(item);
        }
    }
}