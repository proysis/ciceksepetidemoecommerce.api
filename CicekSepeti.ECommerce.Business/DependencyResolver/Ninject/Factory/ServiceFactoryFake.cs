using Ninject;

namespace CicekSepeti.ECommerce.Business.DependencyResolver.Ninject.Factory
{
    public class ServiceFactoryFake : IServiceFactory
    {
        private readonly IKernel _kernel;
        
        public ServiceFactoryFake()
        {
            _kernel = new StandardKernel();
            _kernel.Load<BusinessModuleFake>();
        }

        public TService GetService<TService>()
        {
            return _kernel.Get<TService>();
        }

        public void Release<TService>(TService item)
        {
            _kernel?.Release(item);
        }
    }
}