using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using CicekSepeti.ECommerce.Business.Abstract;
using CicekSepeti.ECommerce.Core.Extensions;
using CicekSepeti.ECommerce.Entity.Concrete.Tables;
using CicekSepeti.ECommerce.Entity.Concrete.Views;

namespace CicekSepeti.ECommerce.Business.Fake
{
    public class BasketProductServiceFake : IBasketProductService
    {
        public List<BasketProduct> GetListByBasketId(Guid basketId)
        {
            return FakeDatas.BasketProducts.Where(t => t.BasketId == basketId).ToList();
        }

        public void AddOrUpdateBasketProduct(BasketProduct basketProduct)
        {
            BasketProduct newBasketProduct = FakeDatas.BasketProducts.SingleOrDefault(t =>
                t.BasketId == basketProduct.BasketId && t.ProductId == basketProduct.ProductId);

            if (newBasketProduct.HasValue())
            {
                // ReSharper disable once PossibleNullReferenceException
                newBasketProduct.BasketId = basketProduct.BasketId;
                newBasketProduct.Amount = basketProduct.Amount;
                newBasketProduct.ProductId = basketProduct.ProductId;
                newBasketProduct.Price = basketProduct.Price;
            }
            else
            {
                Add(basketProduct);
            }
        }

        public BasketProduct GetBasketProduct(Guid basketId, Guid productId)
        {
            return FakeDatas.BasketProducts.SingleOrDefault(t => t.BasketId == basketId && t.ProductId == productId);
        }

        public ViewBasketProduct GetBasketProductView(Guid userId, Guid productId)
        {
            return FakeDatas.ViewBasketProducts.SingleOrDefault(t => t.UserId == userId && t.ProductId == productId);
        }

        public void AddListToBasket(List<BasketProduct> basketProducts, Guid basketId)
        {
            List<BasketProduct> basketProductList =
                FakeDatas.BasketProducts.Where(t => t.BasketId == basketId).ToList();

            foreach (BasketProduct basketProduct in basketProductList)
            {
                BasketProduct newBasketProduct =
                    basketProducts.SingleOrDefault(t => t.BasketId == basketProduct.BasketId);
                if (newBasketProduct.HasValue())
                {
                    // ReSharper disable once PossibleNullReferenceException
                    basketProduct.BasketId = newBasketProduct.BasketId;
                    basketProduct.Amount = newBasketProduct.Amount;
                    basketProduct.ProductId = newBasketProduct.ProductId;
                    basketProduct.Price = newBasketProduct.Price;
                }
                else
                {
                    FakeDatas.BasketProducts.Add(newBasketProduct);
                }
            }
        }

        public List<BasketProduct> GetAll()
        {
            return FakeDatas.BasketProducts;
        }

        public Task<BasketProduct> GetByIdAsync(Guid id)
        {
            throw new NotImplementedException();
        }

        public BasketProduct GetById(Guid id)
        {
            throw new NotImplementedException();
        }

        public BasketProduct GetById(Guid id, Expression<Func<BasketProduct, object>> including)
        {
            throw new NotImplementedException();
        }

        public void Add(BasketProduct item)
        {
            FakeDatas.BasketProducts.Add(item);
        }

        public void Update(BasketProduct item)
        {
            throw new NotImplementedException();
        }

        public void Delete(BasketProduct item)
        {
            throw new NotImplementedException();
        }
    }
}