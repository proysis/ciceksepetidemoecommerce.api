using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using CicekSepeti.ECommerce.Business.Abstract;
using CicekSepeti.ECommerce.Core.Extensions;
using CicekSepeti.ECommerce.Entity.Concrete.Tables;

namespace CicekSepeti.ECommerce.Business.Fake
{
    public class BasketServiceFake : IBasketService
    {
        public Task<Basket> GetBasketByUserIdAsync(Guid userId)
        {
            return Task.Run(() => FakeDatas.BasketList.SingleOrDefault(t => t.UserId == userId));
        }

        public Basket GetBasketByUserId(Guid userId)
        {
            return FakeDatas.BasketList.SingleOrDefault(t => t.UserId == userId);
        }

        public Basket SetBasket(Guid userId)
        {
            Basket basket = FakeDatas.BasketList.SingleOrDefault(t => t.UserId == userId);
            if (basket.HasValue())
                return basket;
            
            return new Basket
            {
                Id = FakeDatas.BasketId,
                UserId = Guid.Empty,
                Date = DateTime.Now
            };
        }

        public List<Basket> GetAll()
        {
            return FakeDatas.BasketList;
        }

        public Task<Basket> GetByIdAsync(Guid id)
        {
            return Task.Run(() => FakeDatas.BasketList.SingleOrDefault(t => t.Id == id));
        }

        public Basket GetById(Guid id)
        {
            return FakeDatas.BasketList.SingleOrDefault(t => t.Id == id);
        }

        public Basket GetById(Guid id, Expression<Func<Basket, object>> including)
        {
            throw new NotImplementedException();
        }

        public void Add(Basket item)
        {
            throw new NotImplementedException();
        }

        public void Update(Basket item)
        {
            throw new NotImplementedException();
        }

        public void Delete(Basket item)
        {
            throw new NotImplementedException();
        }
    }
}