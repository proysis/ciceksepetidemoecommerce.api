using System;
using System.Collections.Generic;
using System.Linq;
using CicekSepeti.ECommerce.Entity.Concrete.Tables;
using CicekSepeti.ECommerce.Entity.Concrete.Views;

namespace CicekSepeti.ECommerce.Business.Fake
{
    public static class FakeDatas
    {
        public static Guid BasketId = Guid.NewGuid();
        public static Guid ProductId1 = Guid.NewGuid();
        public static Guid ProductId2 = Guid.NewGuid();
        public const decimal UnitPrice1 = (decimal) 3242.43;
        public const decimal UnitPrice2 = (decimal) 2315.58;
        public static Guid FakeUserId = Guid.Parse("03af8464-1a17-441c-826d-4af76ade0d5a");

        public static readonly List<Basket> BasketList = new List<Basket>
        {
            new Basket
            {
                Id = BasketId,
                UserId = FakeUserId,
                Date = DateTime.Now
            }
        };
            
        public static readonly List<Product> Products = new List<Product>
        {
            new Product
            {
                Id = ProductId1,
                Name = "Test Product",
                Description = "Test Product Description",
                Stock = 12,
                UnitPrice = UnitPrice1
            },
            new Product
            {
                Id = ProductId2,
                Name = "Test Product 2",
                Description = "Test Product Description 2",
                Stock = 20,
                UnitPrice = UnitPrice2
            }
        };

        public static readonly List<BasketProduct> BasketProducts = new List<BasketProduct>
        {
            new BasketProduct
            {
                BasketId = BasketId,
                ProductId = ProductId1,
                Amount = 5,
                Price = 5 * UnitPrice1
            }
        };
        

        public static readonly List<ViewBasketProduct> ViewBasketProducts = new List<ViewBasketProduct>
        {
            new ViewBasketProduct
            {
                UserId = FakeUserId,
                BasketId = BasketId,
                ProductId = ProductId1,
                Amount = 5,
                Price = 5 * UnitPrice1
            }
        };
    }
}