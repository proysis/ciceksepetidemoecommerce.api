using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using CicekSepeti.ECommerce.Business.Abstract;
using CicekSepeti.ECommerce.Entity.Concrete.Tables;

namespace CicekSepeti.ECommerce.Business.Fake
{
    public class ProductServiceFake : IProductService
    {
        public List<Product> GetAll()
        {
            return FakeDatas.Products;
        }

        public Task<Product> GetByIdAsync(Guid id)
        {
            return Task.Run(() => FakeDatas.Products.SingleOrDefault(t => t.Id == id));
        }

        public Product GetById(Guid id)
        {
            return FakeDatas.Products.SingleOrDefault(t => t.Id == id);
        }

        public Product GetById(Guid id, Expression<Func<Product, object>> including)
        {
            throw new NotImplementedException();
        }

        public void Add(Product item)
        {
            throw new NotImplementedException();
        }

        public void Update(Product item)
        {
            throw new NotImplementedException();
        }

        public void Delete(Product item)
        {
            throw new NotImplementedException();
        }
    }
}