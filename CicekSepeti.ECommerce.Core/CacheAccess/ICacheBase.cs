namespace CicekSepeti.ECommerce.Core.CacheAccess
{
    public interface ICacheBase
    {
        void Add(string key, object value);
        TModel GetByKey<TModel>(string key) where TModel : class;

        void Delete(string key);
    }
}