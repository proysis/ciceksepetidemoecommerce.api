using System;
using Microsoft.Extensions.Caching.Memory;

namespace CicekSepeti.ECommerce.Core.CacheAccess.Memory
{
    public class MemoryCacheBase : ICacheBase
    {
        private readonly IMemoryCache _memoryCache = new MemoryCache(new MemoryCacheOptions());


        public void Add(string key, object value)
        {
            using (ICacheEntry cache = _memoryCache.CreateEntry(key))
            {
                cache.Value = value;
                cache.AbsoluteExpiration = DateTime.Now.AddDays(1);
            }
        }

        public TModel GetByKey<TModel>(string key) where TModel : class
        {
            bool hasValue = _memoryCache.TryGetValue(key, out TModel value);
            return hasValue ? value :  default ;
        }

        public void Delete(string key)
        { 
            _memoryCache.Remove(key);
        }
    }
}