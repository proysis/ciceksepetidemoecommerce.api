using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using StackExchange.Redis;

namespace CicekSepeti.ECommerce.Core.CacheAccess.Redis
{
    public class RedisCacheBase : ICacheBase
    {
        private readonly ConnectionMultiplexer _redis = ConnectionMultiplexer.Connect("127.0.0.1:6379");

        private readonly IDatabase redisCache;

        protected RedisCacheBase()
        {
            redisCache = _redis.GetDatabase();
        }

        public void Add(string key, object value)
        {
            redisCache.StringSet(key, JsonConvert.SerializeObject(value));
        }

        public TModel GetByKey<TModel>(string key) where TModel : class
        {
            string value = redisCache.StringGet(key);

            return value == null ? default : JsonConvert.DeserializeObject<TModel>(value);
        }

        public void Delete(string key)
        {
            redisCache.KeyDelete(key);
        }
    }
}