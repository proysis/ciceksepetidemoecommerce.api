using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using CicekSepeti.ECommerce.Core.Entities;

namespace CicekSepeti.ECommerce.Core.DataAccess
{
    public interface IEntityRepository<T> where T : class, IEntity, new()
    {
        Task<T> GetAsync(Expression<Func<T, bool>> filter);
        T Get(Expression<Func<T, bool>> filter);
        T GetWithIncluding(Expression<Func<T, bool>> filter, Expression<Func<T, object>> including);

        List<T> GetList(Expression<Func<T, bool>> filter = null);

        void Add(T entity);
        void AddRange(List<T> entityList);

        void Update(T entity);

        void Delete(T entity);

        void DeleteRange(List<T> entityList);

        bool Any(Expression<Func<T, bool>> filter = null);
    }
}