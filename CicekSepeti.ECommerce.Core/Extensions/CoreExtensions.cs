using System;
using System.Globalization;
using System.Security.Cryptography;
using System.Text;

namespace CicekSepeti.ECommerce.Core.Extensions
{
    public static class CoreExtensions
    {
        public static TResult GetPropertyValue<T, TResult>(this T item, string propertyName) where T : class
        {
            return (TResult) typeof(T).GetProperty(propertyName).GetValue(item);
        }

        public static TResult GetPropertyValue<TResult>(this Object item, string propertyName)
        {
            return (TResult) item.GetType().GetProperty(propertyName).GetValue(item);
        }

        public static string GetPropertyValueAsString<T>(this T item, string propertyName) where T : class
        {
            return (string) item.GetType().GetProperty(propertyName).GetValue(item);
        }

        public static void SetPropertyValue<T, TValue>(this T item, string propertyName, TValue propertyValue)
            where T : class
        {
            item.GetType().GetProperty(propertyName).SetValue(item, propertyValue);
        }

        public static string ToMd5(this string text)
        {
            byte[] result = new byte[text.Length];
            MD5 md = new MD5CryptoServiceProvider();
            UTF8Encoding encode = new UTF8Encoding();
            result = md.ComputeHash(encode.GetBytes(text));

            return BitConverter.ToString(result).Replace("-", "");
        }

        public static Guid ToGuid(this string guid)
        {
            return Guid.Parse(guid);
        }

        public static byte ToByte(this object value)
        {
            return Convert.ToByte(value);
        }

        public static int ToInt32(this object value)
        {
            return Convert.ToInt32(value);
        }

        public static string BindNullable(this string item)
        {
            return !string.IsNullOrEmpty(item) ? item : null;
        }

        public static string GetCultureSimpleName(this CultureInfo item)
        {
            return item.Name.Substring(0, 2);
        }

        public static string ShowDecimal(this decimal val)
        {
            return val.ToString("##.00");
        }

        public static bool HasValue<T>(this T item)
        {
            return item != null;
        }

        public static bool IsEmpty(this Guid item)
        {
            return item == Guid.Empty;
        }
    }
}