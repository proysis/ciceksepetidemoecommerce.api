using System.Text;
using Microsoft.AspNetCore.Http;

namespace CicekSepeti.ECommerce.Core.Extensions
{
    public static class SessionExtensions
    {
        public static void SetString(this ISession session, string key, string value)
        {
            session.Set(key, Encoding.UTF8.GetBytes(value));
        }

        public static string GetString(this ISession session, string key)
        {
            byte[] bytes = session.Get(key);
            return bytes == null ? null : Encoding.UTF8.GetString(bytes);
        }

        public static byte[] Get(this ISession session, string key)
        {
            session.TryGetValue(key, out var numArray);
            return numArray;
        }
    }
}