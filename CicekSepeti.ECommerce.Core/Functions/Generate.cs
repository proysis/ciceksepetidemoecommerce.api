using System;
using System.Text;

namespace CicekSepeti.ECommerce.Core.Functions
{
    public class Generate
    { 
        private const string Chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        public static string Random(int size = 10, bool lowerCase = false) 
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            for (var i = 0; i < size; i++)
            {
                var ch = Chars[(Convert.ToInt32(Math.Floor(36 * random.NextDouble())))];
                builder.Append(ch);
            }
            return lowerCase ? builder.ToString().ToLower() : builder.ToString();
        }
    }
}