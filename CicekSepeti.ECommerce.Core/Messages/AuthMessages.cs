using System.Runtime.InteropServices;

namespace CicekSepeti.ECommerce.Core.Messages
{
    [StructLayout(LayoutKind.Sequential, Size = 1)]
    public struct AuthMessages
    {
        public const string UnauthorizedAccess = "Unauthorized access.";
        public const string UnVerifiedUser = "Online user is not verified. Please log-in again.";
        public const string NoOnlineUser = "There is no online user.";
        public const string UserNotFound = "User not found.";
    }
}