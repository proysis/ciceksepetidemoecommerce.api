using System.Runtime.InteropServices;

namespace CicekSepeti.ECommerce.Core.Messages
{
    [StructLayout(LayoutKind.Sequential, Size = 1)]
    public struct ErrorMessages
    {
        public const string CompareAttributeMustMatch = "'{0}' and '{1}' do not match.";

        public const string EmailAddressAttributeInvalid = "The {0} field is not a valid e-mail address.";

        public const string RequiredAttributeValidationError = "The {0} field is required.";

        public const string LimitExceeded = "Stock limit has been exceeded.";
        
        public const string ProductNotFound = "Product could not found.";
        public const string ValueNotZero = "The {0} field must not be zero.";
        public const string ValueNotMinus = "The {0} field must not be minus value.";
    }
}