using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using CicekSepeti.ECommerce.Core.DataAccess;
using CicekSepeti.ECommerce.Core.Entities;

namespace CicekSepeti.ECommerce.DataAccess.Abstract
{
    public interface IBaseDal<TEntity> : IEntityRepository<TEntity> where TEntity : class, IEntity, new()
    {
        List<TView> GetViewList<TView>(Expression<Func<TView, bool>> filter = null)
            where TView : class, IEntity, new();
        TView GetView<TView>(Expression<Func<TView, bool>> filter)
            where TView : class, IEntity, new();
    }
}