using CicekSepeti.ECommerce.Entity.Concrete.Tables;

namespace CicekSepeti.ECommerce.DataAccess.Abstract
{
    public interface IBasketDal : IBaseDal<Basket>
    {
        
    }
}