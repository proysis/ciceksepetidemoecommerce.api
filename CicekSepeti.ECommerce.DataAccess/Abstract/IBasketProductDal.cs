using System;
using System.Collections.Generic;
using CicekSepeti.ECommerce.Entity.Concrete.Tables;
using CicekSepeti.ECommerce.Entity.Models;

namespace CicekSepeti.ECommerce.DataAccess.Abstract
{
    public interface IBasketProductDal : IBaseDal<BasketProduct>
    {
        void AddProductToBasket(BasketProduct basketProduct, Guid userId);

        void UpdateAmount(BasketProduct basketProduct);

        void AddProductListToBasket(List<BasketProduct> basketProducts, Guid basketId);
    }
}