using Microsoft.SqlServer.Management.Smo;

namespace CicekSepeti.ECommerce.DataAccess.Abstract
{
    public interface IDataBase
    {
        void Create();
        bool Has(Server server, string controlScript);
    }
}