using CicekSepeti.ECommerce.Entity.Concrete.Tables;

namespace CicekSepeti.ECommerce.DataAccess.Abstract
{
    public interface IUserSecurityRoleDal : IBaseDal<UserSecurityRole>
    {
    }
}