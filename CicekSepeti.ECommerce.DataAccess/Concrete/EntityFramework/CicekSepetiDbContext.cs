using CicekSepeti.ECommerce.Entity.Concrete.Tables;
using CicekSepeti.ECommerce.Entity.Concrete.Views;
using Microsoft.EntityFrameworkCore;

namespace CicekSepeti.ECommerce.DataAccess.Concrete.EntityFramework
{
    public class CicekSepetiDbContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(DatabaseConfig.ConnectionString);
        }
        
        public DbSet<User> User { get; set; }
        public DbSet<SecurityRole> SecurityRole { get; set; }
        public DbSet<UserSecurityRole> UserSecurityRole { get; set; }
        public DbSet<BasketProduct> BasketProduct { get; set; }
        public DbSet<Basket> Basket { get; set; }
        public DbSet<Product> Product { get; set; }
        
        
        public DbSet<ViewUserSecurityRole> ViewUserSecurityRole { get; set; }
        public DbSet<ViewBasketProduct> ViewBasketProduct { get; set; }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<UserSecurityRole>().HasKey(table => new {
                table.SecurityRoleId, table.UserId
            });
            
            builder.Entity<BasketProduct>().HasKey(table => new {
                table.BasketId, table.ProductId
            });
            
            builder.Entity<ViewUserSecurityRole>().HasKey(table => new {
                table.SecurityRoleId, table.UserId
            });
            
            builder.Entity<ViewBasketProduct>().HasKey(table => new {
                table.BasketId, table.ProductId
            });
        }
    }
}