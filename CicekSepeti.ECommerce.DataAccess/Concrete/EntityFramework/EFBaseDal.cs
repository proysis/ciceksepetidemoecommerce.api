using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using CicekSepeti.ECommerce.Core.DataAccess.EntityFramework;
using CicekSepeti.ECommerce.Core.Entities;
using CicekSepeti.ECommerce.DataAccess.Abstract;

namespace CicekSepeti.ECommerce.DataAccess.Concrete.EntityFramework
{
    public class EFBaseDal<TEntity> : EfEntityRepositoryBase<TEntity, CicekSepetiDbContext>, IBaseDal<TEntity> where TEntity : class, IEntity, new()
    {
        public List<TView> GetViewList<TView>(Expression<Func<TView, bool>> filter = null) where TView : class, IEntity, new()
        {
            using (CicekSepetiDbContext context = new CicekSepetiDbContext())
            {
                return filter != null ? context.Set<TView>().Where(filter).ToList() : context.Set<TView>().ToList();
            }
        }

        public TView GetView<TView>(Expression<Func<TView, bool>> filter) where TView : class, IEntity, new()
        {
            using (CicekSepetiDbContext context = new CicekSepetiDbContext())
            {
                return context.Set<TView>().SingleOrDefault(filter);
            }
        }
    }
}