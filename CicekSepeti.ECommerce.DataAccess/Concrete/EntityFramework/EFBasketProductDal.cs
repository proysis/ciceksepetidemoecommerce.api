using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Transactions;
using CicekSepeti.ECommerce.Core.Extensions;
using CicekSepeti.ECommerce.DataAccess.Abstract;
using CicekSepeti.ECommerce.Entity.Concrete.Tables;
using CicekSepeti.ECommerce.Entity.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

namespace CicekSepeti.ECommerce.DataAccess.Concrete.EntityFramework
{
    public class EFBasketProductDal : EFBaseDal<BasketProduct>, IBasketProductDal
    {
        public void AddProductToBasket(BasketProduct basketProduct, Guid userId)
        {
            using (CicekSepetiDbContext context = new CicekSepetiDbContext())
            {
                using (IDbContextTransaction transaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        Basket basket = new Basket
                        {
                            UserId = userId,
                            Date = DateTime.Now
                        };

                        context.Basket.Add(basket);
                        context.SaveChanges();

                        basketProduct.BasketId = basket.Id;
                        context.BasketProduct.Add(basketProduct);
                        context.SaveChanges();

                        transaction.Commit();
                    }
                    catch (SqlException)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }
        }

        public void UpdateAmount(BasketProduct basketProduct)
        {
            using (CicekSepetiDbContext context = new CicekSepetiDbContext())
            {
                basketProduct.Amount = basketProduct.Amount;
            }
        }

        public void AddProductListToBasket(List<BasketProduct> basketProducts, Guid basketId)
        {
            using (CicekSepetiDbContext context = new CicekSepetiDbContext())
            {
                List<BasketProduct> basketProductList =
                    context.BasketProduct.Where(t => t.BasketId == basketId).AsNoTracking().ToList();

                List<BasketProduct> updateBasketProducts =
                    basketProducts.Where(t => basketProductList.Any(x => x.ProductId == t.ProductId)).ToList();

                if (updateBasketProducts.Any())
                    context.BasketProduct.UpdateRange(updateBasketProducts);

                List<BasketProduct> addedBasketProducts =
                    basketProducts.Where(t => basketProductList.All(x => x.ProductId != t.ProductId)).ToList();

                if (addedBasketProducts.Any())
                    context.BasketProduct.AddRange(addedBasketProducts);

                context.SaveChanges();
            }
        }
    }
}