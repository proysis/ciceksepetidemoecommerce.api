using System;
using System.Data.SqlClient;
using System.IO;
using CicekSepeti.ECommerce.Core.Extensions;
using CicekSepeti.ECommerce.DataAccess.Abstract;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;

namespace CicekSepeti.ECommerce.DataAccess.Concrete.EntityFramework
{
    public class EFDataBase : IDataBase
    {
        private const string ServerConnectionString = DatabaseConfig.Server;

        private const string ControlScript =
            "SELECT CASE WHEN EXISTS(SELECT NULL FROM master.sys.databases WHERE name = N'ECommerceDemoDB') THEN 1 ELSE 0 END";

        public void Create()
        {
            FileInfo file = new FileInfo("Databases/ECommerceDemoDB.sql");
            string script = file.OpenText().ReadToEnd();
            SqlConnection connection = new SqlConnection(ServerConnectionString);
            Server server = new Server(new ServerConnection(connection));
            if (!Has(server, ControlScript))
                server.ConnectionContext.ExecuteNonQuery(script);
        }

        public bool Has(Server server, string controlScript)
        {
            return Convert.ToBoolean(server.ConnectionContext.ExecuteScalar(controlScript).ToInt32());
        }
    }
}