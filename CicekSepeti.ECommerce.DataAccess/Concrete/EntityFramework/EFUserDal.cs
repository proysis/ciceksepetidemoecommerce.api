

using CicekSepeti.ECommerce.DataAccess.Abstract;
using CicekSepeti.ECommerce.Entity.Concrete.Tables;

namespace CicekSepeti.ECommerce.DataAccess.Concrete.EntityFramework
{
    public class EFUserDal : EFBaseDal<User>, IUserDal
    {
        
    }
}