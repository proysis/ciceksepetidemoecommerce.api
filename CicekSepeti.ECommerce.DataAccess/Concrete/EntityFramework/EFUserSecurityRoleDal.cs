using System;
using System.Collections.Generic;
using CicekSepeti.ECommerce.DataAccess.Abstract;
using CicekSepeti.ECommerce.Entity.Concrete.Tables;

namespace CicekSepeti.ECommerce.DataAccess.Concrete.EntityFramework
{
    public class EFUserSecurityRoleDal: EFBaseDal<UserSecurityRole>, IUserSecurityRoleDal
    {
    }
}