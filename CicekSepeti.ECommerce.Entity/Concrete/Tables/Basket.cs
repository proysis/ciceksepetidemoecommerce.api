using System;
using System.Collections.Generic;
using CicekSepeti.ECommerce.Core.Entities;

namespace CicekSepeti.ECommerce.Entity.Concrete.Tables
{
    public class Basket : IEntity
    {
        public Basket()
        {
            BasketProducts = new List<BasketProduct>();
        }
        
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public DateTime Date { get; set; }

        public User User { get; set; }
        public List<BasketProduct> BasketProducts { get; set; }
    }
    
    
}