using System;
using System.Collections.Generic;
using CicekSepeti.ECommerce.Core.Entities;

namespace CicekSepeti.ECommerce.Entity.Concrete.Tables
{
    public class BasketProduct : IEntity
    {
        public Guid BasketId { get; set; }
        public Guid ProductId { get; set; }
        public int Amount { get; set; }
        public decimal Price { get; set; }

        public Basket Basket { get; set; }
        public Product Product { get; set; }
    }
}