using System;
using System.Collections.Generic;
using CicekSepeti.ECommerce.Core.Entities;

namespace CicekSepeti.ECommerce.Entity.Concrete.Tables
{
    public class Product : IEntity
    {
        public Product()
        {
            BasketProducts = new List<BasketProduct>();
        }
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal UnitPrice { get; set; }
        public int Stock { get; set; }

        public List<BasketProduct> BasketProducts { get; set; }
    }
}