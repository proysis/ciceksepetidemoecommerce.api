using System;
using System.Collections.Generic;
using CicekSepeti.ECommerce.Core.Entities;

namespace CicekSepeti.ECommerce.Entity.Concrete.Tables
{
    public class SecurityRole : IEntity
    {
        public SecurityRole()
        {
            UserSecurityRoles = new List<UserSecurityRole>();
        }
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }

        public List<UserSecurityRole> UserSecurityRoles { get; set; }
    }
}