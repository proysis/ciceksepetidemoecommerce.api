using System;
using System.Collections.Generic;
using CicekSepeti.ECommerce.Core.Entities;

namespace CicekSepeti.ECommerce.Entity.Concrete.Tables
{
    public class User : IEntity
    {
        public User()
        {
            UserSecurityRoles = new List<UserSecurityRole>();
        }
        
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string UserToken { get; set; }
        
        public List<UserSecurityRole> UserSecurityRoles { get; set; }
    }
    
    
}