using System;
using CicekSepeti.ECommerce.Core.Entities;

namespace CicekSepeti.ECommerce.Entity.Concrete.Tables
{
    public class UserSecurityRole : IEntity
    {
        public Guid SecurityRoleId { get; set; }
        public Guid UserId { get; set; }

        public User User { get; set; }
        public SecurityRole SecurityRole { get; set; }
    }
}