using System;
using System.ComponentModel.DataAnnotations;
using CicekSepeti.ECommerce.Core.Entities;

namespace CicekSepeti.ECommerce.Entity.Concrete.Views
{
    public class ViewUserSecurityRole : IEntity
    {
        [Key]
        public Guid SecurityRoleId { get; set; }
        [Key]
        public Guid UserId { get; set; }
        public string Code { get; set; }
    }
}