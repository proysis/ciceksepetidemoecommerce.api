using Microsoft.AspNetCore.Mvc.DataAnnotations;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using Microsoft.Extensions.Localization;

namespace CicekSepeti.ECommerce.Entity.ModelCustomValidations.Adapters
{
    public class NotMinusAttributeAdapter : AttributeAdapterBase<NotMinusAttribute>
    {
        public NotMinusAttributeAdapter(NotMinusAttribute attribute, IStringLocalizer stringLocalizer) : base(attribute, stringLocalizer)
        {
        }

        public override string GetErrorMessage(ModelValidationContextBase validationContext)
        {
            return GetErrorMessage(validationContext.ModelMetadata, validationContext.ModelMetadata.GetDisplayName());
        }

        public override void AddValidation(ClientModelValidationContext context)
        {
        }
    }
}