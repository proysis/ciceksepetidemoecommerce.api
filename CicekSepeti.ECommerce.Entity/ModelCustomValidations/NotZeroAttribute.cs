using System;
using System.ComponentModel.DataAnnotations;

namespace CicekSepeti.ECommerce.Entity.ModelCustomValidations
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter)]
    public class NotZeroAttribute : ValidationAttribute
    {
        private const string DefaultErrorMessage = "The field must not be zero";
        public NotZeroAttribute() : base(DefaultErrorMessage) { }
 
        public override bool IsValid(object value)
        {
            if (value is null)
            {
                return true;
            }

            switch (value)
            {
                case int integer:
                    return integer != 0;
                default:
                    return true;
            }
        }
    }
}