using System;

namespace CicekSepeti.ECommerce.Entity.Models
{
    public class BasketProductModel
    {

        public Guid BasketId { get; set; }
        public Guid ProductId { get; set; }
        public int Amount { get; set; }
        public decimal Price { get; set; }
    }
}