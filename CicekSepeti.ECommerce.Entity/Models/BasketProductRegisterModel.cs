using System;
using System.ComponentModel.DataAnnotations;
using CicekSepeti.ECommerce.Core.Messages;
using CicekSepeti.ECommerce.Entity.ModelCustomValidations;

namespace CicekSepeti.ECommerce.Entity.Models
{
    public class BasketProductRegisterModel
    {
        public Guid ProductId { get; set; }
        [NotZero(ErrorMessage = ErrorMessages.ValueNotZero)]
        [NotMinus(ErrorMessage = ErrorMessages.ValueNotMinus)]
        [Display(Name = "Amount")]
        public int Amount { get; set; }
    }
}