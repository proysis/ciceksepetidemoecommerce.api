using System.ComponentModel.DataAnnotations;
using CicekSepeti.ECommerce.Core.Messages;

namespace CicekSepeti.ECommerce.Entity.Models
{
    public class LoginModel
    {
        
        [Required(ErrorMessage = ErrorMessages.RequiredAttributeValidationError)]
        [Display(Name = "User Name")]
        public string UserName { get; set; }
        
        [Required(ErrorMessage = ErrorMessages.RequiredAttributeValidationError)]
        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}