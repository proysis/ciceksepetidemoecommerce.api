using System;
using System.Collections.Generic;
using CicekSepeti.ECommerce.Entity.Concrete.Views;

namespace CicekSepeti.ECommerce.Entity.Models
{
    public class UserModel
    {
        public UserModel()
        {
            ViewUserSecurityRoles = new List<ViewUserSecurityRole>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }

        public List<ViewUserSecurityRole> ViewUserSecurityRoles { get; set; }
    }
}