using System.ComponentModel.DataAnnotations;
using CicekSepeti.ECommerce.Core.Messages;

namespace CicekSepeti.ECommerce.Entity.Models
{
    public class UserRegisterModel
    {
        [Required(ErrorMessage = ErrorMessages.RequiredAttributeValidationError)]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Required(ErrorMessage = ErrorMessages.RequiredAttributeValidationError)]
        [Display(Name = "Surname")]
        public string Surname { get; set; }

        [Required(ErrorMessage = ErrorMessages.RequiredAttributeValidationError)]
        [Display(Name = "User Name")]
        public string UserName { get; set; }

        [Required(ErrorMessage = ErrorMessages.RequiredAttributeValidationError)]
        [EmailAddress(ErrorMessage = ErrorMessages.EmailAddressAttributeInvalid)]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = ErrorMessages.RequiredAttributeValidationError)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required(ErrorMessage = ErrorMessages.RequiredAttributeValidationError)]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = ErrorMessages.CompareAttributeMustMatch)]
        [Display(Name = "Password Confirm")]
        public string PasswordConfirm { get; set; }
    }
}