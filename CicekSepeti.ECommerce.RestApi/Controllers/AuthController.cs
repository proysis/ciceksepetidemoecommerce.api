using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using CicekSepeti.ECommerce.Business.Abstract;
using CicekSepeti.ECommerce.Business.CacheManager.Abstract;
using CicekSepeti.ECommerce.Business.DependencyResolver.Ninject.Factory;
using CicekSepeti.ECommerce.Core.Extensions;
using CicekSepeti.ECommerce.Core.Functions;
using CicekSepeti.ECommerce.Core.Messages;
using CicekSepeti.ECommerce.Entity.Concrete.Tables;
using CicekSepeti.ECommerce.Entity.Concrete.Views;
using CicekSepeti.ECommerce.Entity.Models;
using CicekSepeti.ECommerce.RestApi.GlobalResources;
using CicekSepeti.ECommerce.RestApi.Validations;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Localization;

namespace CicekSepeti.ECommerce.RestApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : BaseController
    {

        private readonly IMapper _mapper;
        private IStringLocalizer<AuthResources> _localizer;
        
        public AuthController(IServiceFactory serviceFactory, IMapper mapper, IStringLocalizer<AuthResources> localizer) : base(serviceFactory)
        {
            _mapper = mapper;
            _localizer = localizer;
        }

        [ValidateModel]
        [HttpPost("login")]
        [PassToBasket]
        public async Task<IActionResult> LogIn(LoginModel loginModel)
        {
            IUserService userService = ServiceFactory.GetService<IUserService>();
            User user = await userService.GetByUserNameAndPassword(loginModel.UserName, loginModel.Password);

            if (!user.HasValue()) return Unauthorized(_localizer[AuthMessages.UserNotFound]);

            user.UserToken = Generate.Random(50, true);
            userService.Update(user);
            
            List<ViewUserSecurityRole> viewUserSecurityRoles =
                ServiceFactory.GetService<IUserSecurityRoleService>().GetUserSecurityRoles(user.Id);
            
            UserModel userModel = _mapper.Map<UserModel>(user);
            userModel.ViewUserSecurityRoles = viewUserSecurityRoles;
            
            OnlineUser = userModel;
            ServiceFactory.GetService<ICacheService>().SetNewUserToken(OnlineUser.Id, user.UserToken);

            return Ok(user.UserToken);
        }

        
        [HttpPost("logout")]
        public async Task<IActionResult> LogOut()
        {
            if (OnlineUser.Id.IsEmpty()) return NotFound();

            new HttpContextAccessor().HttpContext.Session.Remove(UserType.OnlineUser);
            
            return Ok();
        }
        
        
         ///<summary>Gets current online user</summary>
         /// <remarks>Just for current session</remarks>
         
        [HttpGet("current")]
        [ProducesResponseType(200, Type = typeof(UserModel))]
        public IActionResult GetCurrentUser()
        {
            if (OnlineUser.Id.IsEmpty()) return NotFound(_localizer[AuthMessages.NoOnlineUser]);
            
            return Ok(OnlineUser);
        }
    }
}