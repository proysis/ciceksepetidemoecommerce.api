using System.Collections.Generic;
using CicekSepeti.ECommerce.Business.CacheManager.Abstract;
using CicekSepeti.ECommerce.Business.DependencyResolver.Ninject.Factory;
using CicekSepeti.ECommerce.Business.Fake;
using CicekSepeti.ECommerce.Core.Extensions;
using CicekSepeti.ECommerce.Entity.Models;
using CicekSepeti.ECommerce.RestApi.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;

namespace CicekSepeti.ECommerce.RestApi.Controllers
{
    public class BaseController : ControllerBase
    {
        protected readonly IServiceFactory ServiceFactory;
        public readonly IHttpContextAccessor _contextAccessor = new HttpContextAccessor();
        private ISession _session => _contextAccessor.HttpContext.Session;

        public bool IsTest = false;
        public bool TestOnline = false;

        protected bool HasOnlineUser => IsTest ? TestOnline : !OnlineUser.Id.IsEmpty();

        public BaseController(IServiceFactory serviceFactory)
        {
            ServiceFactory = serviceFactory;
        }
        protected UserModel OnlineUser
        {
            get => IsTest ? new UserModel{Id = FakeDatas.FakeUserId} : _session.GetOnlineUser();
            set => _session.SetOnlineUser(value);
        }

        protected List<BasketProductModel> TempBasket
        {
            get => _session.GetTempBasket();
            set => _session.SetTempBasket(value);
        }
        
        

    }
}