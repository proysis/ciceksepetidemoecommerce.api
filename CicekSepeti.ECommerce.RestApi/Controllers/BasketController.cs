using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CicekSepeti.ECommerce.Business.Abstract;
using CicekSepeti.ECommerce.Business.DependencyResolver.Ninject.Factory;
using CicekSepeti.ECommerce.Core.Extensions;
using CicekSepeti.ECommerce.Entity.Concrete.Tables;
using CicekSepeti.ECommerce.Entity.Models;
using CicekSepeti.ECommerce.RestApi.Validations;
using Microsoft.AspNetCore.Mvc;

namespace CicekSepeti.ECommerce.RestApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ConfirmOnlineUser]
    public class BasketController : BaseController
    {
        public BasketController(IServiceFactory serviceFactory) : base(serviceFactory)
        {
        }

        [HttpPost("add-product-to-basket")]
        [HasStock]
        public IActionResult AddProductToBasket([FromBody] BasketProductRegisterModel basketProductModel)
        {
            IProductService productService = ServiceFactory.GetService<IProductService>();
            Product product = productService.GetById(basketProductModel.ProductId);
            if (HasOnlineUser)
            {
                IBasketService basketService = ServiceFactory.GetService<IBasketService>();
                IBasketProductService basketProductService = ServiceFactory.GetService<IBasketProductService>();

                Basket basket = basketService.SetBasket(OnlineUser.Id);
                
                BasketProduct newBasketProduct = new BasketProduct
                {
                    ProductId = product.Id,
                    Amount = 0,
                    Price = 0
                };

                List<BasketProduct> basketProducts = basketProductService.GetListByBasketId(basket.Id)?? new List<BasketProduct>();
                
                BasketProduct basketProduct =
                    basketProducts.SingleOrDefault(t => t.ProductId == product.Id) ?? newBasketProduct;

                basketProduct.BasketId = basket.Id;
                basketProduct.Amount += basketProductModel.Amount;
                basketProduct.Price = basketProduct.Amount * product.UnitPrice;
                
                basketProductService.AddOrUpdateBasketProduct(basketProduct);

                return Ok();
            }

            List<BasketProductModel> productModels = TempBasket;
            BasketProductModel productModel = productModels.SingleOrDefault(t=>t.ProductId == basketProductModel.ProductId);
            if (productModel.HasValue())
            {
                // ReSharper disable once PossibleNullReferenceException
                productModel.Amount += basketProductModel.Amount;
                productModel.Price = productModel.Amount * product.UnitPrice;
            }
            else
            {
                productModels.Add(new BasketProductModel
                {
                    ProductId = basketProductModel.ProductId,
                    Amount = basketProductModel.Amount,
                    Price = basketProductModel.Amount * product.UnitPrice
                });
            }

            TempBasket = productModels;

            return Ok();
        }
    }
}