using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using CicekSepeti.ECommerce.Business.Abstract;
using CicekSepeti.ECommerce.Business.DependencyResolver.Ninject.Factory;
using CicekSepeti.ECommerce.Core.Extensions;
using CicekSepeti.ECommerce.Entity.Concrete.Tables;
using CicekSepeti.ECommerce.Entity.Models;
using CicekSepeti.ECommerce.RestApi.Validations;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CicekSepeti.ECommerce.RestApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ConfirmOnlineUser]
    public class BasketProductController : BaseController
    {
        public BasketProductController(IServiceFactory serviceFactory) : base(serviceFactory)
        {
        }

        [HttpGet]
        public async Task<IActionResult> GetBasketProduct()
        {
            IBasketProductService basketProductService = ServiceFactory.GetService<IBasketProductService>();

            IBasketService basketService = ServiceFactory.GetService<IBasketService>();

            Basket basket = await basketService.GetBasketByUserIdAsync(OnlineUser.Id);

            if (basket.HasValue())
                return Ok(basketProductService.GetListByBasketId(basket.Id));

            return NotFound("Sepet bulunamadı");
        }

        [HttpGet("temp-basket")]
        public IActionResult GetTempBasketProduct()
        {
            return Ok(TempBasket);
        }

        [HttpPost("reduce-product-from-basket")]
        public IActionResult ReduceBasketProduct(BasketProductReduceModel basketProductReduceModel)
        {
            Product product = ServiceFactory.GetService<IProductService>()
                .GetById(basketProductReduceModel.ProductId);
            if (HasOnlineUser)
            {
                IBasketProductService basketProductService = ServiceFactory.GetService<IBasketProductService>();
                BasketProduct basketProduct = basketProductService.GetBasketProduct(basketProductReduceModel.BasketId,
                    basketProductReduceModel.ProductId);
                if (!basketProduct.HasValue()) return NotFound();

                basketProduct.Amount -= basketProductReduceModel.Amount;
                basketProduct.Price = basketProduct.Amount * product.UnitPrice;

                if (basketProduct.Amount <= 0)
                    basketProductService.Delete(basketProduct);
                else
                    basketProductService.Update(basketProduct);

                return Ok();
            }

            List<BasketProductModel> productModels = TempBasket;
            BasketProductModel basketProductModel =
                productModels.SingleOrDefault(t => t.ProductId == basketProductReduceModel.ProductId);
            if (!basketProductModel.HasValue()) return NotFound();

            // ReSharper disable once PossibleNullReferenceException
            basketProductModel.Amount -= basketProductReduceModel.Amount;
            basketProductModel.Price = basketProductModel.Amount * product.UnitPrice;

            if (basketProductModel.Amount <= 0)
                productModels.Remove(basketProductModel);

            TempBasket = productModels;

            return Ok();
        }
    }
}