using CicekSepeti.ECommerce.Business.Abstract;
using CicekSepeti.ECommerce.Business.DependencyResolver.Ninject.Factory;
using CicekSepeti.ECommerce.RestApi.Validations;
using Microsoft.AspNetCore.Mvc;

namespace CicekSepeti.ECommerce.RestApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ConfirmOnlineUser]
    public class ProductController : BaseController
    {
        public ProductController(IServiceFactory serviceFactory) : base(serviceFactory)
        {
        }

        [HttpGet]
        public IActionResult GetProductList()
        {
            IProductService productService = ServiceFactory.GetService<IProductService>();
            return Ok(productService.GetAll());
        }
    }
}