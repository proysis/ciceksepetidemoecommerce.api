using System;
using CicekSepeti.ECommerce.Business.DependencyResolver.Ninject.Factory;
using CicekSepeti.ECommerce.RestApi.GlobalResources;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace CicekSepeti.ECommerce.RestApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SettingController : BaseController
    {
        
        private readonly IStringLocalizer<SharedResources> _sharedLocalizer;
        
        public SettingController(IStringLocalizer<SharedResources> sharedLocalizer, IServiceFactory factory) : base(factory)
        {
            _sharedLocalizer = sharedLocalizer;
        }
        /// <summary>
        ///     It changes culture for localization
        /// </summary>
        /// <remarks>available cultures: en-US, tr-TR</remarks>
        /// <param name="culture">en-US or tr-TR</param>
        /// <returns></returns>
        [HttpPost("change-culture")]
        public IActionResult ChangeCulture(string culture)
        {
            Response.Cookies.Append(
                CookieRequestCultureProvider.DefaultCookieName,
                CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(culture)),
                new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1) }
            );

            return Ok(_sharedLocalizer["Name"]);
        }
    }
}