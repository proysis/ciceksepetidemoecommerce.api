using System;
using System.Threading.Tasks;
using AutoMapper;
using CicekSepeti.ECommerce.Business.Abstract;
using CicekSepeti.ECommerce.Business.DependencyResolver.Ninject.Factory;
using CicekSepeti.ECommerce.Core.Extensions;
using CicekSepeti.ECommerce.Core.Functions;
using CicekSepeti.ECommerce.Entity.Concrete.Tables;
using CicekSepeti.ECommerce.Entity.Models;
using CicekSepeti.ECommerce.RestApi.Data;
using CicekSepeti.ECommerce.RestApi.GlobalResources;
using CicekSepeti.ECommerce.RestApi.Validations;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Localization;

namespace CicekSepeti.ECommerce.RestApi.Controllers
{
    /// <summary>
    /// Admin is required.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [AuthRequired(Role = Roles.Admin)]
    public class UserController : BaseController
    {

        private readonly IMapper _mapper;

        private readonly IStringLocalizer<SharedResources> _sharedLocalizer;
        
        public UserController(IServiceFactory serviceFactory, IMapper mapper, IStringLocalizer<SharedResources> sharedLocalizer) : base(serviceFactory)
        {
            _mapper = mapper;
            _sharedLocalizer = sharedLocalizer;
        }
        
        [ValidateModel]
        [HttpPost("add")]
        public IActionResult AddUser([FromBody] UserRegisterModel userRegisterModel)
        {
            string userToken = Generate.Random(50, true);

            User user = _mapper.Map<User>(userRegisterModel);
            
            user.UserToken = userToken;

            user.Password = user.Password.ToMd5();
            
            try
            {
                IUserService userService = ServiceFactory.GetService<IUserService>();
                userService.Add(user);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return Ok(user);
        }
        
        [HttpGet]
        public IActionResult GetUser([FromQuery(Name = "userId")] Guid? userId = null)
        {
            IUserService userService = ServiceFactory.GetService<IUserService>();
            return userId.HasValue ? Ok(userService.GetByIdAsync(userId.Value)) : Ok(userService.GetAll());
        }

        [HttpGet("delete/{userId}")]
        public async Task<IActionResult> DeleteUser([FromRoute] Guid userId)
        {
            IUserService userService = ServiceFactory.GetService<IUserService>();
            User user = await userService.GetByIdAsync(userId);
            
            if (!user.HasValue()) return NotFound();
            
            userService.Delete(user);
            return Ok(user.Id);

        }
    }
}