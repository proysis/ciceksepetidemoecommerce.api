﻿-- Create a new database called 'ECommerceDemoDB'
-- Connect to the 'master' database to run this snippet
USE master
GO
-- Create the new database if it does not exist already
IF NOT EXISTS (
	SELECT name
		FROM sys.databases
		WHERE name = N'ECommerceDemoDB'
)
BEGIN
	CREATE DATABASE ECommerceDemoDB
END
ELSE
BEGIN
	DROP DATABASE ECommerceDemoDB
	CREATE DATABASE ECommerceDemoDB
END

GO

USE [ECommerceDemoDB]
GO
/****** Object:  Table [dbo].[UserSecurityRole]    Script Date: 9/12/19 10:01:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserSecurityRole](
	[SecurityRoleId] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SecurityRole]    Script Date: 9/12/19 10:01:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SecurityRole](
	[Id] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Code] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[ViewUserSecurityRole]    Script Date: 9/12/19 10:01:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ViewUserSecurityRole] AS

SELECT 
        usr.*,
        sr.Code 
        FROM UserSecurityRole usr
        INNER JOIN SecurityRole sr ON sr.Id = usr.SecurityRoleId

GO
/****** Object:  Table [dbo].[Basket]    Script Date: 9/12/19 10:01:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Basket](
	[Id] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[Date] [datetime] NOT NULL,
	[Total] [decimal](16, 2) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BasketProduct]    Script Date: 9/12/19 10:01:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BasketProduct](
	[BasketId] [uniqueidentifier] NOT NULL,
	[ProductId] [uniqueidentifier] NOT NULL,
	[Amount] [int] NOT NULL,
	[Price] [decimal](16, 2) NOT NULL,
 CONSTRAINT [UQ_BasketId_ProductId] PRIMARY KEY CLUSTERED 
(
	[BasketId] ASC,
	[ProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[ViewBasketProduct]    Script Date: 9/12/19 10:01:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ViewBasketProduct] AS

SELECT 
        bp.*,
        b.UserId 
        FROM BasketProduct bp
        INNER JOIN Basket b ON b.Id = bp.BasketId
GO
/****** Object:  Table [dbo].[Product]    Script Date: 9/12/19 10:01:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product](
	[Id] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](500) NULL,
	[UnitPrice] [decimal](16, 2) NOT NULL,
	[Stock] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 9/12/19 10:01:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[Id] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Surname] [nvarchar](100) NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[Email] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](50) NOT NULL,
	[UserToken] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Basket] ([Id], [UserId], [Date], [Total]) VALUES (N'ebca070d-0239-41f0-dc71-08d737b07bbc', N'66a7abb1-3746-4858-d1c2-08d737a4574b', CAST(N'2019-09-12T21:39:02.580' AS DateTime), NULL)
INSERT [dbo].[Basket] ([Id], [UserId], [Date], [Total]) VALUES (N'26f5fb73-97fc-42b3-ab55-45c624c10cf7', N'858716f4-dc2c-4c29-b265-08d73158eea9', CAST(N'2019-09-07T19:54:09.357' AS DateTime), NULL)
INSERT [dbo].[BasketProduct] ([BasketId], [ProductId], [Amount], [Price]) VALUES (N'26f5fb73-97fc-42b3-ab55-45c624c10cf7', N'5f2d44ea-2849-4613-87c1-051a1f4b27eb', 5, CAST(11520.00 AS Decimal(16, 2)))
INSERT [dbo].[BasketProduct] ([BasketId], [ProductId], [Amount], [Price]) VALUES (N'26f5fb73-97fc-42b3-ab55-45c624c10cf7', N'2bec3f8e-e2e1-478f-9e43-70974bf64dbc', 1, CAST(3547.57 AS Decimal(16, 2)))
INSERT [dbo].[Product] ([Id], [Name], [Description], [UnitPrice], [Stock]) VALUES (N'5f2d44ea-2849-4613-87c1-051a1f4b27eb', N'Notebook', N'Mobil PC', CAST(2304.00 AS Decimal(16, 2)), 12)
INSERT [dbo].[Product] ([Id], [Name], [Description], [UnitPrice], [Stock]) VALUES (N'2bec3f8e-e2e1-478f-9e43-70974bf64dbc', N'Computer', N'Office Item', CAST(3547.57 AS Decimal(16, 2)), 20)
INSERT [dbo].[Product] ([Id], [Name], [Description], [UnitPrice], [Stock]) VALUES (N'3056babe-4d54-40d3-bfc9-7584ac6a1ce9', N'Phone', N'Office Item', CAST(1254.23 AS Decimal(16, 2)), 10)
INSERT [dbo].[SecurityRole] ([Id], [Name], [Code]) VALUES (N'87a05a08-c403-49dc-ae43-41267c459c11', N'Admin', N'ROLEADMIN')
INSERT [dbo].[User] ([Id], [Name], [Surname], [UserName], [Email], [Password], [UserToken]) VALUES (N'858716f4-dc2c-4c29-b265-08d73158eea9', N'Alp Eren', N'Güngör', N'Proysis', N'gungor.ae@gmail.com', N'C4CA4238A0B923820DCC509A6F75849B', N'dcnr5yt8uo7wzıı2cuwrhwsqgg38q5b5f9r3ng6a65ıa1zıf5g')
INSERT [dbo].[User] ([Id], [Name], [Surname], [UserName], [Email], [Password], [UserToken]) VALUES (N'66a7abb1-3746-4858-d1c2-08d737a4574b', N'Furkan', N'Kaplan', N'Aiken', N'furkn@kpln.com', N'C4CA4238A0B923820DCC509A6F75849B', N'vıuqkq9lzj4ags6jqg0yqk304lldd13jjqqjhlc07v2z4lwtza')
INSERT [dbo].[UserSecurityRole] ([SecurityRoleId], [UserId]) VALUES (N'87a05a08-c403-49dc-ae43-41267c459c11', N'858716f4-dc2c-4c29-b265-08d73158eea9')
/****** Object:  Index [UQ_UserId]    Script Date: 9/12/19 10:01:49 PM ******/
ALTER TABLE [dbo].[Basket] ADD  CONSTRAINT [UQ_UserId] UNIQUE NONCLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [UQ_UserId_SecurityId]    Script Date: 9/12/19 10:01:49 PM ******/
ALTER TABLE [dbo].[UserSecurityRole] ADD  CONSTRAINT [UQ_UserId_SecurityId] UNIQUE NONCLUSTERED 
(
	[UserId] ASC,
	[SecurityRoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Basket]  WITH CHECK ADD  CONSTRAINT [FK_Basket_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[Basket] CHECK CONSTRAINT [FK_Basket_User]
GO
ALTER TABLE [dbo].[BasketProduct]  WITH CHECK ADD  CONSTRAINT [FK_BasketProduct_Basket] FOREIGN KEY([BasketId])
REFERENCES [dbo].[Basket] ([Id])
GO
ALTER TABLE [dbo].[BasketProduct] CHECK CONSTRAINT [FK_BasketProduct_Basket]
GO
ALTER TABLE [dbo].[BasketProduct]  WITH CHECK ADD  CONSTRAINT [FK_BasketProduct_Product] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Product] ([Id])
GO
ALTER TABLE [dbo].[BasketProduct] CHECK CONSTRAINT [FK_BasketProduct_Product]
GO
USE [master]
GO
ALTER DATABASE [ECommerceDemoDB] SET  READ_WRITE 
GO