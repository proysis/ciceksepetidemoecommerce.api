using CicekSepeti.ECommerce.Business.Abstract;
using CicekSepeti.ECommerce.Business.DependencyResolver.Ninject.Factory;
using Microsoft.AspNetCore.Builder;

namespace CicekSepeti.ECommerce.RestApi.Extensions
{
    public static class MiddlewareExtensions
    {
        public static void UseDataBase(this IApplicationBuilder builder)
        {
           IServiceFactory serviceFactory = (IServiceFactory)builder.ApplicationServices.GetService(typeof(IServiceFactory));
           serviceFactory.GetService<IDataBaseService>().CreateDataBase();
        }
    }
}