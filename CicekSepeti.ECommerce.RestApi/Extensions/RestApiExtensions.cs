using CicekSepeti.ECommerce.Entity.Models;

namespace CicekSepeti.ECommerce.RestApi.Extensions
{
    public static class RestApiExtensions
    {
        public static bool HasRole(this UserModel userModel, string roleCode)
        {
            return userModel.ViewUserSecurityRoles.Exists(t => t.Code == roleCode);
        }
        public static bool HasAnyRole(this UserModel userModel, string[] Roles)
        {
            foreach (var item in Roles)
            {
                if (userModel.ViewUserSecurityRoles.Exists(t => t.Code == item))
                {
                    return true;
                }
            }

            return false;
        }
    }
}