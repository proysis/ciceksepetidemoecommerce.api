using System.Collections.Generic;
using CicekSepeti.ECommerce.Entity.Models;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace CicekSepeti.ECommerce.RestApi.Extensions
{
    public static class SessionExtensions
    {
        private static void SetObjectAsJson(this ISession session, string key, object value)
        {
            session.SetString(key, JsonConvert.SerializeObject(value));
        }

        private static T GetObjectFromJson<T>(this ISession session, string key)
        {
            string value = session.GetString(key);

            return value == null ? default : JsonConvert.DeserializeObject<T>(value);
        }

        public static UserModel GetOnlineUser(this ISession session)
        {
            return session.GetObjectFromJson<UserModel>(UserType.OnlineUser) ?? new UserModel();
        }

        public static void SetOnlineUser(this ISession session, UserModel user)
        {
            session.SetObjectAsJson(UserType.OnlineUser, user);
        }
        
        public static string GetUserToken(this ISession session)
        {
            return session.GetObjectFromJson<string>(TokenType.UserToken);
        }

        public static void SetUserToken(this ISession session, string userToken)
        {
            session.SetObjectAsJson(TokenType.UserToken, userToken);
        }
        
        public static List<BasketProductModel> GetTempBasket(this ISession session)
        {
            return session.GetObjectFromJson<List<BasketProductModel>>(BasketType.TempBasket) ?? new List<BasketProductModel>();
        }

        public static void SetTempBasket(this ISession session, List<BasketProductModel> basketProducts)
        {
            session.SetObjectAsJson(BasketType.TempBasket, basketProducts);
        }
    }
}