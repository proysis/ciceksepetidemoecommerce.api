using AutoMapper;
using CicekSepeti.ECommerce.Entity.Concrete.Tables;
using CicekSepeti.ECommerce.Entity.Concrete.Views;
using CicekSepeti.ECommerce.Entity.Models;

namespace CicekSepeti.ECommerce.RestApi.Profiles
{
    public class MappingProfiles : Profile
    {
        public MappingProfiles()
        {
            AllowNullCollections = true;
            
            CreateMap<UserModel,User>();
            CreateMap<User,UserModel>();
            
            CreateMap<User,UserRegisterModel>();
            CreateMap<UserRegisterModel,User>();
            
            CreateMap<ViewBasketProduct,BasketProductModel>();
            CreateMap<BasketProductRegisterModel,BasketProductModel>();
        }
    }
}