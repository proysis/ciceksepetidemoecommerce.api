using System.ComponentModel.DataAnnotations;
using CicekSepeti.ECommerce.Entity.ModelCustomValidations;
using CicekSepeti.ECommerce.Entity.ModelCustomValidations.Adapters;
using Microsoft.AspNetCore.Mvc.DataAnnotations;
using Microsoft.Extensions.Localization;

namespace CicekSepeti.ECommerce.RestApi.Providers
{
    public class CustomValidationAttributeAdapterProvider : IValidationAttributeAdapterProvider
    {
        private readonly IValidationAttributeAdapterProvider _baseProvider = new ValidationAttributeAdapterProvider();

        public IAttributeAdapter GetAttributeAdapter(ValidationAttribute attribute, IStringLocalizer stringLocalizer)
        {
            switch (attribute)
            {
                case NotZeroAttribute zeroAttribute:
                    return new NotZeroAttributeAdapter(zeroAttribute, stringLocalizer);
                case NotMinusAttribute minusAttribute:
                    return new NotMinusAttributeAdapter(minusAttribute, stringLocalizer);
                default:
                    return _baseProvider.GetAttributeAdapter(attribute, stringLocalizer);
            }
        }
    }
}