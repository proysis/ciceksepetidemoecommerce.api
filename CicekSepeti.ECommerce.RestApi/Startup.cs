﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using AutoMapper;
using CicekSepeti.ECommerce.Business.DependencyResolver.Ninject.Factory;
using CicekSepeti.ECommerce.RestApi.Extensions;
using CicekSepeti.ECommerce.RestApi.GlobalResources;
using CicekSepeti.ECommerce.RestApi.Providers;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;

namespace CicekSepeti.ECommerce.RestApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddLocalization(o => { o.ResourcesPath = "Resources"; });

            //Session için redis server kullanmak istiyorsanız.
//            services.AddDistributedRedisCache(option =>
//            {
//                option.Configuration = "localhost:6379"; // Redis server ip
//                option.InstanceName = "RedisDB";
//            });

            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromDays(1); //You can set Time   
            });

            services.AddHttpContextAccessor();

            // Auto Mapper Configurations
            services.AddAutoMapper();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("CoreSwagger", new OpenApiInfo
                {
                    Title = "Swagger on ASP.NET Core",
                    Version = "1.0.0",
                    Description = "Try Swagger on (ASP.NET Core 2.2)",
                    TermsOfService = new Uri("http://swagger.io/terms/")
                });

                string xmlFile = Path.ChangeExtension(typeof(Startup).Assembly.Location, ".xml");
                c.IncludeXmlComments(xmlFile);
            });
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddViewLocalization(LanguageViewLocationExpanderFormat.Suffix).AddDataAnnotationsLocalization(
                    opt =>
                    {
                        opt.DataAnnotationLocalizerProvider =
                            (type, factory) => factory.Create(typeof(SharedResources));
                    });
            services.AddSingleton<IServiceFactory, ServiceFactory>();
            services.AddSingleton<IValidationAttributeAdapterProvider, CustomValidationAttributeAdapterProvider>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseSession();

            IList<CultureInfo> supportedCultures = new List<CultureInfo>
            {
                new CultureInfo("en-US") {NumberFormat = {CurrencyDecimalSeparator = "."}},
                new CultureInfo("tr-TR") {NumberFormat = {CurrencyDecimalSeparator = ","}}
            };

            RequestLocalizationOptions localizationOptions = new RequestLocalizationOptions
            {
                DefaultRequestCulture = new RequestCulture("en-US"),
                SupportedCultures = supportedCultures,
                SupportedUICultures = supportedCultures
            };
            app.UseRequestLocalization(localizationOptions);
            app.UseSwagger()
                .UseSwaggerUI(c => { c.SwaggerEndpoint("/swagger/CoreSwagger/swagger.json", "Swagger .Net Core"); });
            app.UseHttpsRedirection();
            app.UseDataBase();
            app.UseMvc();
        }
    }
}