namespace CicekSepeti.ECommerce.RestApi
{
    public static class UserType
    {
        public static readonly string OnlineUser = "ONLINEUSER";
    }
    public static class TokenType
    {
        public static readonly string UserToken = "USERTOKEN";
    }

    public static class BasketType
    {
        public static readonly string TempBasket = "TEMPBASKET";
    }
}