using System;
using CicekSepeti.ECommerce.Business.Abstract;
using CicekSepeti.ECommerce.Business.CacheManager.Abstract;
using CicekSepeti.ECommerce.Business.CacheManager.Concrete;
using CicekSepeti.ECommerce.Business.DependencyResolver.Ninject.Factory;
using CicekSepeti.ECommerce.Core.Extensions;
using CicekSepeti.ECommerce.Core.Messages;
using CicekSepeti.ECommerce.RestApi.Extensions;
using CicekSepeti.ECommerce.RestApi.GlobalResources;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Localization;

namespace CicekSepeti.ECommerce.RestApi.Validations
{
    public class AuthRequiredAttribute : ActionFilterAttribute
    {
        private readonly IHttpContextAccessor _httpContextAccessor = new HttpContextAccessor();
        private ISession Session => _httpContextAccessor.HttpContext.Session;
        private readonly Type _localizerType = typeof(IStringLocalizer<AuthResources>);
        private readonly Type _serviceFactoryType = typeof(IServiceFactory);
        private IServiceFactory _serviceFactory;


        public string[] Roles { get; set; }
        public string Role { get; set; }

        /// <inheritdoc />
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            _serviceFactory =
                (IServiceFactory) filterContext.HttpContext.RequestServices.GetService(_serviceFactoryType);
            IStringLocalizer localizer =
                (IStringLocalizer) filterContext.HttpContext.RequestServices.GetService(_localizerType);

            Guid userId = Session.GetOnlineUser().Id;

            if (userId.IsEmpty())
            {
                filterContext.Result = new UnauthorizedObjectResult(localizer[AuthMessages.NoOnlineUser]);
                return;
            }

            string key = CacheType.UserToken + userId;
            ICacheService cacheService = _serviceFactory.GetService<ICacheService>();
            bool userIsValid = _serviceFactory.GetService<IUserService>()
                .ValidaUser(userId, cacheService.GetUserToken(key));

            if (!userIsValid)
            {
                filterContext.Result = new UnauthorizedObjectResult(localizer[AuthMessages.UnVerifiedUser]);
                return;
            }

            if (!string.IsNullOrEmpty(Role) && !Session.GetOnlineUser().HasRole(Role))
            {
                filterContext.Result = new UnauthorizedObjectResult(localizer[AuthMessages.UnauthorizedAccess]);
                return;
            }

            if (Roles != null && Roles.Any() && !Session.GetOnlineUser().HasAnyRole(Roles))
            {
                filterContext.Result = new UnauthorizedObjectResult(localizer[AuthMessages.UnauthorizedAccess]);
            }
        }
    }
}