using System;
using System.Linq;
using AutoMapper;
using CicekSepeti.ECommerce.Business.Abstract;
using CicekSepeti.ECommerce.Business.CacheManager.Abstract;
using CicekSepeti.ECommerce.Business.DependencyResolver.Ninject.Factory;
using CicekSepeti.ECommerce.Core;
using CicekSepeti.ECommerce.Core.Extensions;
using CicekSepeti.ECommerce.Core.Messages;
using CicekSepeti.ECommerce.Entity.Concrete.Tables;
using CicekSepeti.ECommerce.Entity.Models;
using CicekSepeti.ECommerce.RestApi.Extensions;
using CicekSepeti.ECommerce.RestApi.GlobalResources;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Localization;

namespace CicekSepeti.ECommerce.RestApi.Validations
{
    public class HasStockAttribute : ActionFilterAttribute
    {

        private readonly IHttpContextAccessor _httpContextAccessor = new HttpContextAccessor();
        private ISession Session => _httpContextAccessor.HttpContext.Session;

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            
            IServiceFactory serviceFactory =
                (IServiceFactory) filterContext.HttpContext.RequestServices.GetService(typeof(IServiceFactory));
            
            IStringLocalizer localizer = (IStringLocalizer) filterContext.HttpContext.RequestServices.GetService(typeof(IStringLocalizer<ResponseResources>));
            
            IMapper mapper = (IMapper) filterContext.HttpContext.RequestServices.GetService(typeof(IMapper));

            BasketProductRegisterModel basketProductRegisterModel = (BasketProductRegisterModel) filterContext.ActionArguments.First().Value;

            Guid productId = basketProductRegisterModel.ProductId;

            IProductService productService = serviceFactory.GetService<IProductService>();

            Product product = productService.GetById(productId);

            Guid userId = Session.GetOnlineUser().Id;
            BasketProductModel basketProductModel = userId.IsEmpty()
                ? mapper.Map<BasketProductModel>(Session.GetTempBasket().SingleOrDefault(t=>t.ProductId == productId))
                : mapper.Map<BasketProductModel>(serviceFactory.GetService<IBasketProductService>()
                    .GetBasketProductView(userId, productId)); 

            
            // ReSharper disable once PossibleNullReferenceException
            int amount = basketProductRegisterModel.Amount + (basketProductModel.HasValue() ? basketProductModel.Amount : 0);

            if (!product.HasValue())
            {
                filterContext.Result = new NotFoundObjectResult(localizer[ErrorMessages.ProductNotFound]);
            }
            else if (product.Stock < amount)
            {
                filterContext.Result = new BadRequestObjectResult(localizer[ErrorMessages.LimitExceeded]);
            }
        }
    }
}