using System.Collections.Generic;
using System.Linq;
using CicekSepeti.ECommerce.Business.Abstract;
using CicekSepeti.ECommerce.Business.DependencyResolver.Ninject.Factory;
using CicekSepeti.ECommerce.Core.Extensions;
using CicekSepeti.ECommerce.Entity.Concrete.Tables;
using CicekSepeti.ECommerce.Entity.Models;
using CicekSepeti.ECommerce.RestApi.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;

namespace CicekSepeti.ECommerce.RestApi.Validations
{
    public class PassToBasketAttribute : ActionFilterAttribute
    {
        public override void OnResultExecuted(ResultExecutedContext executedContext)
        {
            base.OnResultExecuted(executedContext);

            ISession session = new HttpContextAccessor().HttpContext.Session;
            List<BasketProductModel> productModels = session.GetTempBasket();
            UserModel onlineUser = session.GetOnlineUser();

            if (onlineUser.Id.IsEmpty() || !productModels.Any()) return;
            
            IServiceFactory serviceFactory =
                (IServiceFactory) executedContext.HttpContext.RequestServices.GetService(typeof(IServiceFactory));

            Basket basket = serviceFactory.GetService<IBasketService>().SetBasket(onlineUser.Id);

            List<BasketProduct> basketProducts = new List<BasketProduct>();
            foreach (BasketProductModel basketProductRegisterModel in productModels)
            {
                basketProducts.Add(new BasketProduct
                {
                    BasketId = basket.Id,
                    ProductId = basketProductRegisterModel.ProductId,
                    Amount = basketProductRegisterModel.Amount,
                    Price = basketProductRegisterModel.Price
                });
            }
                
            serviceFactory.GetService<IBasketProductService>().AddListToBasket(basketProducts, basket.Id);
            session.Remove(BasketType.TempBasket);
        }
    }
}