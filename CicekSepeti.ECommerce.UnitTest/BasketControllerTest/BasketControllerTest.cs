using CicekSepeti.ECommerce.Business.Abstract;
using CicekSepeti.ECommerce.Business.DependencyResolver.Ninject.Factory;
using CicekSepeti.ECommerce.Business.Fake;
using CicekSepeti.ECommerce.Entity.Concrete.Tables;
using CicekSepeti.ECommerce.Entity.Models;
using CicekSepeti.ECommerce.RestApi.Controllers;
using CicekSepeti.ECommerce.UnitTest.MockData;
using Microsoft.AspNetCore.Mvc;
using Xunit;

namespace CicekSepeti.ECommerce.UnitTest.BasketControllerTest
{
    public class BasketControllerTest
    {
        private readonly BasketController _controller;
        private readonly IServiceFactory _serviceFactory;
        
        public BasketControllerTest()
        {
            _serviceFactory = new ServiceFactoryFake();
            _controller = new BasketController(_serviceFactory);
            _controller._contextAccessor.HttpContext = new HttpContextAccessorMock().HttpContextMock;
            _controller.IsTest = true;
        }
        
        [Fact]
        public void AddProductToBasket_WhenCalled_ReturnOkResult()
        {
            _controller.TestOnline = true;

            BasketProductRegisterModel basketProductRegisterModel = new BasketProductRegisterModel
                {Amount = 15, ProductId = FakeDatas.ProductId2};
            
            IActionResult okResult = _controller.AddProductToBasket(basketProductRegisterModel);

            Assert.IsType<OkResult>(okResult);
            Basket basket = _serviceFactory.GetService<IBasketService>().GetBasketByUserId(FakeDatas.FakeUserId);
            BasketProduct basketProduct = _serviceFactory.GetService<IBasketProductService>()
                .GetBasketProduct(basket.Id, basketProductRegisterModel.ProductId);
            Assert.NotNull(basketProduct);
            Assert.Equal(basketProductRegisterModel.Amount, basketProduct.Amount);
            Assert.Equal(basketProductRegisterModel.ProductId, basketProduct.ProductId);
        }
    }
    
    
}