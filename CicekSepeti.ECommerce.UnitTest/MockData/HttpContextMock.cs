using Microsoft.AspNetCore.Http;
using Moq;

namespace CicekSepeti.ECommerce.UnitTest.MockData
{
    public class HttpContextAccessorMock
    {

        public HttpContext HttpContextMock => GetMockedHttpContext();
        private static HttpContext GetMockedHttpContext()
        {
            Mock<HttpContext> context = new Mock<HttpContext>();
            Mock<ISession> session = new Mock<ISession>();
            
            context.Setup(ctx => ctx.Session).Returns(session.Object);

            return context.Object;
        }
    }
}